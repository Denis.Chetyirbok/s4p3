@isTest
public class TestProductTableApplication {
	@isTest
    private static void Test1TriggerInsertNormalAmount(){
        ProductTable__c pr = new ProductTable__c(
        	Name='product1',
            Amount__c=2,
            Price__c=10,
            ProductType__c='type1',
            ReleaseDate__c=System.today() 
        );
        
        Test.startTest();
        Database.SaveResult dbsr = Database.insert(pr, true);
        List<ProductTable__c> pt = [
            Select Available__c 
            From ProductTable__c
            Where Name='product1' 
        	Limit 1
        ];
        Test.stopTest();
        System.assert(dbsr.isSuccess()); 
        System.assertEquals(true,pt[0].Available__c);
        
    }
    
    @isTest
    private static void Test2TriggerInsertZeroAmount(){
        ProductTable__c pr = new ProductTable__c(
        	Name='product2',
            Amount__c=0,
            Price__c=20,
            ProductType__c='type2',
            ReleaseDate__c=System.today().addDays(-2)
        );
        
        Test.startTest();
        Database.SaveResult dbsr = Database.insert(pr, true);
        List<ProductTable__c> pt = [
            Select Available__c 
            From ProductTable__c
            Where Name='product2' 
        	Limit 1
        ];
        Test.stopTest();
        System.assert(dbsr.isSuccess());
        System.assertEquals(false,pt[0].Available__c);
    }
    
    @isTest
    private static void Test3TriggerUpdateAmount(){
        ProductTable__c pr = new ProductTable__c(
        	Name='product2',
            Amount__c=0,
            Price__c=20,
            ProductType__c='type2',
            ReleaseDate__c=System.today().addDays(-2)
        );
        insert pr;
        pr.Amount__c = 2;
        
        
        Test.startTest();  
        List<ProductTable__c> pt = [
            Select Available__c 
            From ProductTable__c
            Where Name='product2' 
        	Limit 1
        ];
        System.assertEquals(false, pt[0].Available__c);
        Database.SaveResult dbsr = Database.update(pr, true);
        pt = [
            Select Available__c 
            From ProductTable__c
            Where Name='product2' 
        	Limit 1
        ];
        Test.stopTest();
        System.assert(dbsr.isSuccess());
        System.assertEquals(true, pt[0].Available__c);
        
    }


    @isTest
    private static void Test4ControllerSearch(){
        ProductTable__c pr = new ProductTable__c(
            Name='product1',
            Amount__c=2,
            Price__c=8,
            ProductType__c='type1',
            ReleaseDate__c=System.today().addDays(-1)
        );
        insert pr;
        integer d = System.today().Day();
        integer m = System.today().Month();
        integer y = System.today().Year();
        string dateOfCreation = (d<10?'0'+d:d+'') +'.'+ (m<10?'0'+m:m+'')+'.'+y;
        
        string inputName = JSON.serialize(pr.Name);
        string inputDate = JSON.serialize(dateOfCreation);
        string inputEmpty = JSON.serialize('');
        
        Test.startTest();  
		List<ProductTable__c> productInputName = (List<ProductTable__c>)JSON.deserialize(ProductTableCmpController.getProducts(inputName), List<ProductTable__c>.class);
        List<ProductTable__c> productInputDate = (List<ProductTable__c>)JSON.deserialize(ProductTableCmpController.getProducts(inputDate), List<ProductTable__c>.class);
        List<ProductTable__c> productInputEmpty = (List<ProductTable__c>)JSON.deserialize(ProductTableCmpController.getProducts(inputEmpty), List<ProductTable__c>.class);              
        Test.stopTest();
        
        System.assertEquals(pr.Name, productInputName[0].Name);
        System.assertEquals(pr.Name, productInputDate[0].Name);
        System.assertEquals(pr.Name, productInputEmpty[0].Name);
        
    }
	
    @isTest
    private static void Test5ControllerInsertUpdateDelete(){
        ProductTable__c pr = new ProductTable__c(
            Name='product1',
            Amount__c=2,
            Price__c=8,
            ProductType__c='type1',
            ReleaseDate__c=System.today().addDays(-1)
        );
        string stringResult = ProductTableCmpController.insertNewProductTable(JSON.serialize(pr), JSON.serialize(''));
        List<ProductTable__c> productInsert = (List<ProductTable__c>)JSON.deserialize(stringResult, List<ProductTable__c>.class);
        System.assertEquals(pr.Name, productInsert[0].Name);
        
        pr.Name = 'Super Product';
        stringResult = ProductTableCmpController.updateNewProductTable(JSON.serialize(pr), JSON.serialize(productInsert[0].Id), JSON.serialize(''));
        List<ProductTable__c> productUpdate = (List<ProductTable__c>)JSON.deserialize(stringResult, List<ProductTable__c>.class);
        System.assertEquals('Super Product', productUpdate[0].Name);
        
        ProductTable__c pt = new ProductTable__c(
            Name='product2',
            Amount__c=0,
            Price__c=12,
            ProductType__c='type2',
            ReleaseDate__c=System.today().addDays(-3)
        );
        stringResult = ProductTableCmpController.insertNewProductTable(JSON.serialize(pt), JSON.serialize(''));
        List<ProductTable__c> products = (List<ProductTable__c>)JSON.deserialize(stringResult, List<ProductTable__c>.class);
        System.assertEquals(2, products.size());
        stringResult = ProductTableCmpController.deleteProductTable(JSON.serialize(productInsert[0].Id), JSON.serialize(''));
        List<ProductTable__c> productsArterDelete = (List<ProductTable__c>)JSON.deserialize(stringResult, List<ProductTable__c>.class);
        System.assertEquals(1, productsArterDelete.size());
        
    }
    	@isTest
    	private static void Test6ControllerPickList(){  
            string stringResult = ProductTableCmpController.getPickListValues();
            List<string> elements = (List<string>)JSON.deserialize(stringResult, List<string>.class);    	
            System.assertEquals(3, elements.size());            
        }

}