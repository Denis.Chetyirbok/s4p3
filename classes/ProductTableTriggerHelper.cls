public class ProductTableTriggerHelper {
	public static void avaivableCheck(ProductTable__c[] trigg){
        for(ProductTable__c item : trigg){
            if(item.Amount__c > 0){
                item.Available__c=true;
            } else {
                item.Available__c=false;
            }
        }
    }
    public static void addedDateCheck(ProductTable__c[] trigg){
        Datetime dt = Datetime.now();
        for(ProductTable__c item : trigg){
            if(item.AddedDate__c == NULL){
                item.AddedDate__c=dt;
            } 
        }
    }
}